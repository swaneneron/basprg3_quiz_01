#include "Potion.h"
Potion::Potion(string name, double percentage) :Item(name, percentage) {}

Potion::~Potion(){}

void Potion::pull(Character* player, Character* draw) {
	cout << player->getName() << " pulls a " << xName << "!!!" << endl;
	//used pull
	int add = 1;
	player->pullCount(add);
	//reduce crystals
	int takeCrystal = 5;
	player->reduceCrystal(takeCrystal);
	//Heals damage
	int healPts = 30;
	player->heal(healPts);
	cout << player->getName() << " gains " << " 30 Health points!" << endl;
}