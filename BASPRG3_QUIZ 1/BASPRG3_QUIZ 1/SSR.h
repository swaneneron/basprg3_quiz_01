#pragma once
#include "Item.h"
class SSR : public Item
{
public:
	SSR(string name, double percentage);
	~SSR();
	void pull(Character* player, Character* draw);
};