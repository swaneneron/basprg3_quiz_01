
//Implement Player
//This holds the values we need for the game(eg.hp, crystals)
//You might want to add functions to reduce burden on client code 
#include "Character.h"
#include "Item.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "Bomb.h"
#include "Potion.h"
#include "Crystal.h"
using namespace std;
#include <iostream>

Character::Character(string name, int hp, int crystal, Wheel* wheel,int rarePts,int pulls) {
	xName = name;
	xHp = hp;
	xCrystal = crystal;
	xWheel = wheel;
	xRare = rarePts;
	xpullCount = pulls;
	pulls = 0;
	
	//Simulate that the character can draw items from the Wheel
		xItems.push_back(new R("R", 40.0));
  		xItems.push_back(new SR("SR", 9.0));
     	xItems.push_back(new SSR("SSR", 1.0));
		xItems.push_back(new Bomb("Bomb", 20.0));
		xItems.push_back(new Crystal("Crystals", 15.0));
		xItems.push_back(new Potion("Health Potion", 15.0));
		
}

Character::~Character() {
	if (xWheel != NULL)
		delete xWheel;
	for (int i = 0; i < xItems.size(); i++)
		delete xItems[i];
	xItems.clear();
}

void Character::pull(Character* draw) {
	int randIndex = rand() % xItems.size();

	Item* itemToGet = xItems[randIndex];
	if (canPull(itemToGet) == true) {
		itemToGet->pull(this, draw);
	}
	else {
		cout << "You lose!" << endl;
		cout << "You ran out of crystals! GAME OVER" << endl;
		system("pause");
		exit(0);
	}
}


//take damage from bomb
void Character::takeDmg(int value) {
	//only takes damage from bomb pulled
	if (value > 0) {
		xHp -= value;
	}
	//no negative Hp allowed
	if (xHp < 0)
		xHp = 0;
}

//Reduce crystals for pull
void Character::reduceCrystal(int value) {
	//only take  greater than zero here
	if (value > 0) {
		xCrystal -= value;
	}
	//To not allow negative Hp
	if (xHp < 0)
		xHp = 0;
}

//Heal player
void Character::heal(int value) {
	if (value > 0) {
		xHp += value;
	}
}

//character name
string Character::getName() {
	return this->xName;
}

//Character health
int Character::getHp() {
	return this->xHp;
}

//Gain Crystals
int Character::getCrystal(int value) {
	//only take  greater than zero here
	if (value > 0) {
		xCrystal += value;
	}
	//To not allow negative Hp
	if (xHp < 0)
		xHp = 0;
	return this->xRare;
}

//get items from Wheel
Wheel* Character::getWheel() {
	return xWheel;
}

//Gain Rarity Points
int Character::getRarePoints(int value) {
	//only take  greater than zero here
	if (value > 0) {
		xRare += value;
	}
	//To not allow negative Hp
	if (xHp < 0)
		xHp = 0;
	//WHen rarity is met
	else if (xRare >= 100) {
		cout << "You have reached 100 Rarity Points!!" << endl;
		cout << "You win, GAME OVER" << endl;
		system("pause");
		exit(0);
	}
	return this->xRare;
}
//Pulls
int Character:: pullCount(int value) {
	xpullCount += 1;
	return this->xpullCount;
}
//Print stats of Player
void Character::printStats() {
	cout << "Character: " << xName << endl;
	cout << "Health Points: " << xHp << endl;
	cout << "Crystals: " << xCrystal << endl;
	cout << "Rarity Points: " << xRare << endl;
	cout << "Pulls: " << xpullCount + 1 << endl;
}

bool Character::isAlive() {
	if (xHp > 0)
		return true;
	else
		return false;
}

bool Character::canPull(Item* item) {
	if (xCrystal > item->getCrystalCost()) {
		return true;
	}
	else {
		return false;
	}
}