#include "SSR.h"
SSR::SSR(string name, double percentage) :Item(name, percentage) {}

SSR::~SSR(){}

void SSR::pull(Character* player, Character* draw) {
	cout << player->getName() << " pulls a " << xName << "!!!" << endl;
	//used pull
	int add= 1;
	player->pullCount(add);
	//reduce crystals
	int takeCrystal = 5;
	player->reduceCrystal(takeCrystal);
	//gain rarity points
	int point = 50;
	player->getRarePoints(point);
	cout << player->getName() << " gained " << " 50 rarity point!" << endl;
}