#include "SR.h"
SR::SR(string name, double percentage) :Item(name, percentage) {}

SR::~SR(){}

void SR::pull(Character* player, Character* draw) {
	cout << player->getName() << " pulls a " << xName << "!!!" << endl;
	//used pull
	int add = 1;
	player->pullCount(add);
	//reduce crystals
	int takeCrystal = 5;
	player->reduceCrystal(takeCrystal);
	//gain rarity points
	int point = 10;
	player->getRarePoints(point);
	cout << player->getName() << " gained " << " 10 rarity point!" << endl;
}