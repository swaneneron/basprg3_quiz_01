#pragma once
#include "Item.h"
class Potion : public Item
{
public:
	Potion(string name, double percentage);
	~Potion();
	void pull(Character* player, Character* draw);
};