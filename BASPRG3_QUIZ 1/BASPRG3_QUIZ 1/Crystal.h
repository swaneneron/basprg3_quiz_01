#pragma once
#include "Item.h"
class Crystal : public Item
{
public:
	Crystal(string name, double percentage);
	~Crystal();
	void pull(Character* player, Character* draw);
};