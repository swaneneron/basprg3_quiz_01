#include "Bomb.h"
Bomb::Bomb(string name, double percentage) :Item(name, percentage) {}

Bomb::~Bomb(){}

void Bomb::pull(Character* player, Character* draw) {
	cout << player->getName() << " pulls a " << xName << "!!!" << endl;
	//used pull
	int add = 1;
	player->pullCount(add);
	//reduce crystal
	int takeCrystal = 5;
	player->reduceCrystal(takeCrystal);
	//take damage
	int damage = 25;
	player->takeDmg(damage);
	cout << player->getName() << " received " << " 25 damage!" << endl;
}