#include "Item.h"

Item::Item(string name, double percentage) {
	xName = name;
	xPercentage = percentage;
}

Item::~Item(){}

void Item::pull(Character* player, Character* draw) {}

string Item::getName() {
	return xName;
}

int Item::getPercentage() {
	return xPercentage;
}

int Item::getCrystalCost()
{
	return 5;
}
