#pragma once
#include "Item.h"
class R : public Item
{
public:
	R(string name, double percentage);
	~R();
	void pull(Character* player, Character* draw);
};
