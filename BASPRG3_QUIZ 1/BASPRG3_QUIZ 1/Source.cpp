#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Character.h"
#include "Wheel.h"
using namespace std;

int main() {
	srand(time(NULL));
	Character * player = new Character("Vixen", 100, 100, new Wheel,0,0);
	Character* hiden = new Character("", 0, 0, new Wheel,0,0);
	//Game Sequence
	while (player->isAlive()) {
		player->printStats();
		cout << "==============" << endl;
		player->pull(hiden);
		system("pause");
		system("cls");
	}

	//Conditions
	if (player->isAlive()){
		cout << player->getName() << "Wins!" << endl;
		system("pause");
		exit(0);
	}
	if (player->isAlive()==false){
		cout << "You have died.GAME OVER" << endl;
		system("pause");
		exit(0);
	}

}