#pragma once
#include "Item.h"
class Bomb : public Item
{
public:
	Bomb(string name, double percentage);
	~Bomb();
	void pull(Character* player, Character* draw);
};