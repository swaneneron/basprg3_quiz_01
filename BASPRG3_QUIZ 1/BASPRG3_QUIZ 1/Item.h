#pragma once
#include "Character.h"
#include <string>
using namespace std;

class Item {
public:
	Item(string name, double percentage);
	~Item();
	//all children of Items can be pulled
	virtual void pull (Character* player, Character* draw);

	string getName();
	int getPercentage();
	int getCrystalCost();
protected:
	string xName;
	int xPercentage;
};