#pragma once
#include <string>
#include <vector>
#include <iostream>
#include "Wheel.h"
using namespace std;
//avoid circular reference
class Item;
class Character {
public:
	
	Character(string name, int hp, int crystal, Wheel* wheel, int rarePts, int pulls);
	~Character();

	void pull(Character* draw);

	//access method
	void takeDmg(int value);
	void reduceCrystal(int value);
	void heal(int value);

	//getter method
	string getName();
	int getHp();
	int getCrystal(int value);
	Wheel* getWheel();
	int getRarePoints(int value);

	int pullCount(int value);

	//other functions here
	void printStats();
	bool isAlive();
	bool canPull(Item* item);

private:
	string xName;
	int xRare;
	int xHp;
	int xCrystal;
	int xpullCount;
	Wheel* xWheel;
	vector<Item*>xItems;
};